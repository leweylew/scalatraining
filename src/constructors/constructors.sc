// What is a constructor?


/*
     Scala supports two types of constructor

     1. primary
     2. Auxiliary
 */


/*
    PRIMARY CONSTRUCTOR

    When there is only one defined constructor.


    class class_name( class_parameters ){
    }


    It may contain zero or more parameters

*/
class Person(name: String, age: Int)

val person = new Person("JLew", 38)



/*

   The primary constructor and the class SHARE the same body

*/

class Person2(name: String, age: Int){
  println("Beginning of constructor")

  def printDetails(): Unit ={
    println(s"NAME:$name, AGE:$age")
  }

  println("End of constructor")
}

val person2 = new Person2("JLew", 38)
person2.printDetails()



/*

    DEFAULT PRIMARY Constructor

    If no constructor is defined, a default primary one will be created.

*/

class Person3

val person3 = new Person3


/*
    CLASS PARAMETERS VISIBILITY

    var
    val
    default

 */

class PersonVar(var firstName: String, var surname: String)
val personVar = new PersonVar("John", "Lewis")
personVar.firstName = "dsdsds"



class PersonVal(val firstName: String, val surname: String)
val personVal = new PersonVal("John", "Lewis")



class PersonDefault(firstName: String, surname: String)
val personDefault = new PersonDefault("John", "Lewis")




class PersonPrivate(private val firstName:String, surname: String)
val personPrivate = new PersonPrivate("John", "Lewis")



/*
    PRIVATE CONSTRUCTOR
*/

class Singleton private(){
  val initData = init()
  private def init(): String = "initialised"
}

object Singleton {
  private lazy val singleton = new Singleton

  def getInstance(): Singleton = {
    singleton
  }

}

/*
    AUXILIARY

    secondary constructors

 */
class Person4(forename: String, surname: String, preferredName: String){

  def this(forename: String, surname: String){
     this(forename, surname, forename)
  }

  def this(forename: String){
    this(forename, "unknown")
  }
}

val person4 = new Person4("john", "lew", "lou lou")
val person4alt = new Person4("john", "lew")



// CASE CLASS



case class Person5(val forename: String, val surname: String)

val person5 = new Person5("ddsd", "dsdsds")
val person5alt = Person5.apply("dsdsd","dsdsds")

object Person5{
  def apply(name: String, name2:String) = new Person5("overloaded", name2)
}

val alt = Person5.apply("dsds","dsdsd")
alt.forename


// INHERITANCE




class Person6(forename: String, surname: String){
  println("within the person constructor")
}

class Employee(empId: String, forename: String, surname: String)  extends Person6(forename, surname){
  println("Within the employee constructor")
}



println("Constructing employee")
new Employee("NG212121","John","lewis")

// AND THE REST


// objects do not provide a constructor. They represent a single instance of a class. They are created lazily when they
// are referenced like a lazy val
object Person7

// abstract classes



