/*
    THEORY

    Generally, a for expression is of the form:

    for ( seq ) yield expr

    Sequence is a sequence of generators, definitions and filters (or guards)

    However, a for loop is simply Syntactic Sugar in place of higher order functions

 */


/*
   GENERATORS

   pattern or variable <- expression (generally a collection)

*/

val daysOfWeek = List("Monday","Tuesday","Wednesday","Thursday","Friday", "Saturday", "Sunday")

for(day <- daysOfWeek) println(day)

// for multiple lines, perform the work in a block
for(day <- daysOfWeek){
  val upperCaseDay = day.toUpperCase
  println(upperCaseDay)
}


// The above as a higher order function
daysOfWeek.foreach(println(_))


/*
  Filter or GUARD

  If statements in for loops

*/

for(day <- daysOfWeek if day.startsWith("T")) println(day)

// The above using higher order functions
daysOfWeek.withFilter(_.startsWith("T")).foreach(println(_))

/*

    Definitions

    pat = expression

*/

for(day <- daysOfWeek; count = day.length) println(count)

// split across multiple lines
for{
  day <- daysOfWeek
  count = day.length
} println(count)


// how would the above be translated
daysOfWeek.foreach { day =>
  val length = day.length
  println(length)
}

/*
   COUNTERS

   There are times where you may need to access the index of the collection
*/

for(index <- daysOfWeek.indices){
  println(s"$index is ${daysOfWeek(index)}")
}


for((day,index) <- daysOfWeek.zipWithIndex){
  println(s"$index is $day")
}


// multiple counters, using a range like this is referred to as a Generator

for(i <- 1 to 2; j <- 1 to 2){
  println(s"$i, $j ")
}

// across multiple lines

for {
  i <- 1 to 2
  j <- 1 to 2
} println(s"$i, $j ")


// how would the above be translated?
(1 to 2).foreach{ outerLoop =>
  (1 to 2).foreach{ innerLoop =>
    println(s"$outerLoop, $innerLoop")
  }
}

/*
   RETURNING A VALUE


   The above examples don't return a value. In cases where you want to return a
   value use the for / yield combination or For Comprehension.

*/


//Return a new collection with all days being uppercase
val upperCaseWeek = for(day <- daysOfWeek) yield day.toUpperCase


//How would the above be translated?
val upperCaseWeek2 = daysOfWeek.map(_.toUpperCase)



// So when should I use a For Comprehension? Assisting with readability


case class User(id:Int, firstName:String, lastName:String, hobby: Option[String])

object UserRepository{

  private val users =
    Map(1 -> User(1,"J","Lew", Some("drumming")),
        2 -> User(2,"Jay","Bird", None),
        3 -> User(3,"Peter","Pan", Some("karaoke")),
        4 -> User(4,"Naughty","Neda", Some("hiking"))
  )

  def findAll = users.values
}


// Get all hobbies that start with the letter h and make them uppercase
val hobbies = UserRepository.findAll.flatMap{ user =>
  user.hobby.withFilter { hobby =>
    hobby.startsWith("h")
  }.map(hobby => hobby.toUpperCase)
}

//short hand
val hobbiesShortHand =
  UserRepository.findAll.flatMap(_.hobby.withFilter(_.startsWith("h")).map(_.toUpperCase))


// converting to for yield
val hobbiesFor = for {
  user <- UserRepository.findAll
  hobby <- user.hobby
  if hobby.startsWith("h")
} yield hobby.toUpperCase()


//Patterns can be used in the left side of the generators which can act as further filtering
val hobbiesPattern = for {
  User(_, _, _, Some(hobby)) <- UserRepository.findAll
  if hobby.startsWith("h")
} yield hobby.toUpperCase()





















































